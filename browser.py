"""
Selenium based unit tests for Riot Web. Also makes screenshots.
"""

import time
import os
import re

from PIL import Image, PngImagePlugin, ImageDraw

import selenium.webdriver

import config

# Use Firefox in headless mode
os.environ['MOZ_HEADLESS'] = '1'

def firefox(lang='en-us', width=1280, height=1024):
    """Helper to create a firefox browser with the given options"""
    options = selenium.webdriver.firefox.options.Options()
    profile = selenium.webdriver.FirefoxProfile()
    profile.set_preference('intl.accept_languages', lang)
    profile.update_preferences()

    capabilities = selenium.webdriver.DesiredCapabilities().FIREFOX
    capabilities['acceptInsecureCerts'] = True

    driver = selenium.webdriver.Firefox(profile, firefox_options=options, capabilities=capabilities)
    driver.set_window_size(width, height)
    return driver

def chrome(lang='en-us', width=1280, height=1024):
    """Helper to create a chrome browser with the given options"""
    options = selenium.webdriver.ChromeOptions()
    # Note: This sets the browser window size, so the actual content size (and screenshots) will be smaller
    options.add_argument('--window-size=%s,%s'%(width, height))
    options.add_argument('--incognito')

    # Languages are not supported in headless mode
    # See https://bugs.chromium.org/p/chromedriver/issues/detail?id=2154
    # Actually we can't use it even with english, because then we can't ignore TLS :-(
    # Uncomment this once Chrome is fixed, or you have somehow convinced Chrome to trust the server cert
    #if lang.startswith('en'):
    #    options.add_argument('--headless')

    prefs = {'intl.accept_languages': lang}
    options.add_experimental_option('prefs', prefs)

    capabilities = selenium.webdriver.DesiredCapabilities().CHROME
    capabilities['acceptSslCerts'] = True

    driver = selenium.webdriver.Chrome(chrome_options=options, desired_capabilities=capabilities)

    return driver

def path_safe(string, maxlen=200):
    """Make sure a string can be use safely in a file path"""
    return string.replace('/', '_')[:maxlen]

def info_from_dict(metadata):
    """Create a PngInfo from a dictionary"""
    info = PngImagePlugin.PngInfo()
    for key, value in metadata.items():
        info.add_text(key, value)
    return info

class Browser():
    """Class with some convenience methods"""
    def __init__(self, driver, name):
        self.driver = driver
        self.browsername = name
        self.screenshot_dir = 'screenshots/'

    def screenshot(self, task, name, delay=1):
        """Take a screenshot and save it in the right place"""
        try:
            os.makedirs('%s'%self.screenshot_dir)
        except OSError:
            pass # No worries if it already exists. TODO: Check actual error that occurred

        time.sleep(delay) # Sleep a bit to make sure what we want is on screen

        self._string_screenshots(task, name)

        self._screenshot(task, name)

    def _screenshot(self, task, name, additional_metadata=None):
        safe_name = path_safe('%s_%s_%s'%(task, name, self.browsername))
        filename = '%s/%s.png'%(self.screenshot_dir, safe_name)
        self.driver.get_screenshot_as_file(filename)

        # Add metadata to screenshot
        metadata = {
            'browser': self.browsername,
            'task': task,
            'name': name
            }

        if additional_metadata:
            metadata.update(additional_metadata)

        image = Image.open(filename)
        info = info_from_dict(metadata)

        image.save(filename, "png", pnginfo=info)
        return filename


    def click_dropdowns(self, task, name):
        """Click on all dropdowns on a page and take a screenshot"""
        for i, elem in enumerate(self.driver.find_elements_by_class_name('mx_Dropdown') + self.driver.find_elements_by_class_name('mx_NetworkDropdown')):
            elem.click()
            self.screenshot(task, '%s-dropdown-%d'%(name, i))

    def _string_screenshots(self, task, name):
        str_re = re.compile('@@(.*?)##(.*?)@@', re.DOTALL|re.MULTILINE)

        # First replace all texts with spans containing the translated text to make the page appear normal again
        # Then do the same with elements containing translated attributes
        # Finally, take screenshots and mark the location of elements with translated text

        # We can't just do a simple for-loop, because selenium complains about the changing DOM
        # Instead we start from the beginning each time we modified something

        self.driver.implicitly_wait(1)
        modified = True
        while modified:
            modified = False

            for elem in self.driver.find_elements_by_xpath('//*[text()[contains(., "##")]]'):
                inner_html = elem.get_attribute('innerHTML')

                # Wrap content in span
                # It's tempting to just do search and replace in the inner HTML, but that messes up event handlers attached to sibling elements
                # Instead we end up with the following monstrosity
                script = r"""
                    let modified = true;
                    while(modified) {
                        modified = false;
                        var cs = arguments[0].childNodes;
                        for(var i=0; i < cs.length; i++) {
                            if(cs[i].nodeType == Node.TEXT_NODE) {
                                // . does not match newlines in JS, so use \s\S instead
                                let m = cs[i].nodeValue.match(/^([\s\S]*?)@@([\s\S]*?)##([\s\S]*?)@@([\s\S]*)/);
                                if(m) {
                                    var elem = cs[i];

                                    let pre = document.createTextNode(m[1]);
                                    let post = document.createTextNode(m[4])

                                    let span = document.createElement('span');
                                    span.setAttribute('class', 'translated-string');
                                    span.style.boxSizing = 'border-box';
                                    span.setAttribute('data-orig-string', m[2]);

                                    let content = document.createTextNode(m[3]);
                                    span.appendChild(content);

                                    elem.parentNode.insertBefore(pre, elem);
                                    elem.parentNode.insertBefore(span, elem);
                                    elem.parentNode.insertBefore(post, elem);
                                    elem.parentNode.removeChild(elem);

                                    modified = true;

                                    break;
                                }
                            }
                        }
                    }
                """
                self.driver.execute_script(script, elem)
                new_inner_html = elem.get_attribute('innerHTML')

                if new_inner_html != inner_html:
                    modified = True
                    break

        # Handle attributes
        modified = True
        while modified:
            modified = False
            for elem in self.driver.find_elements_by_xpath('//*[@*[contains(., "##")]]'):
                # Get all attributes
                attribs = self.driver.execute_script('var items = {}; for (index = 0; index < arguments[0].attributes.length; ++index) { items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value }; return items;', elem)
                for attrib in attribs:
                    match = str_re.search(attribs[attrib])

                    if match:
                        # Wrap element containing translated attribute
                        script = """
                            arguments[0].setAttribute('%s','%s');
                            span = document.createElement('span');
                            span.setAttribute('class', 'translated-string');
                            span.dataset.processed = 'false';
                            span.setAttribute('data-orig-string', '%s');
                            arguments[0].parentNode.insertBefore(span, arguments[0]);
                            span.appendChild(arguments[0])
                        """%(attrib, match[2].replace("'", '&apos;').replace('\n', '&#10;'), match[1].replace("'", '&apos;').replace('\n', '&#10;'))

                        self.driver.execute_script(script, elem)

                        modified = True
                        break

        # Loop over spans again individually, take screenshot, and mark location
        # Make sure we don't try to process hidden elements
        only_hidden = False
        while not only_hidden:
            only_hidden = True
            # Find all elements that have not been processed yet
            for elem in self.driver.find_elements_by_xpath('//*[@data-orig-string and not(@data-processed="true")]'):
                if not elem.is_displayed():
                    continue # Skip hidden elements
                else:
                    only_hidden = False

                # BUG: Only Firefox and Chrome support this version of scrollIntoView. Investigate how the others behave
                bounds = self.driver.execute_script('arguments[0].scrollIntoView({}); return arguments[0].getBoundingClientRect();', elem)

                additional_metadata = {
                    'orig-string': elem.get_attribute('data-orig-string'),
                    'string-bounds': '%d %d %d %d'%(bounds['left'], bounds['top'], bounds['right'], bounds['bottom'])
                    }

                filename = self._screenshot(task, '%s_string-%s'%(name, elem.get_attribute('data-orig-string')), additional_metadata=additional_metadata)

                # Draw rect
                image = Image.open(filename)
                draw = ImageDraw.Draw(image)
                draw.rectangle([bounds['left'], bounds['top'], bounds['right'], bounds['bottom']], outline=(255, 0, 0, 255))
                draw.rectangle([bounds['left']-1, bounds['top']-1, bounds['right']+1, bounds['bottom']+1], outline=(255, 0, 0, 255))

                info = info_from_dict(image.info)

                image.save(filename, pnginfo=info)

                # Mark element as handled
                self.driver.execute_script('arguments[0].dataset.processed = "true";', elem)

        self.driver.implicitly_wait(config.IMPLICITLY_WAIT)

def browsers():
    """Return iterator over browsers to test"""
    for driver_name, make_driver in config.DRIVERS.items():
        driver = make_driver()
        driver.implicitly_wait(config.IMPLICITLY_WAIT)

        yield Browser(driver, driver_name)
