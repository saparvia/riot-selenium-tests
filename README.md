# Riot-selenium-test
Copyright Stefan Parviainen 2017
Robots lovingly delivered by Robohash.org

Selenium based testing and automatic screenshotting of various features in Riot. Runs test against a Synapse server run with Docker.

The idea is to catch graphical regressions and problems with translations.

![Example image in English](example_en.png)

![Example image in German](example_de.png)

- Install dependencies:
`pip install -r requirements.txt`
(or use your favourite package manager, e.g. `zypper install python3-selenium python3-Pillow`)

- Get geckodriver from https://github.com/mozilla/geckodriver/releases

- Get chromedriver from https://sites.google.com/a/chromium.org/chromedriver/downloads

- Edit config.py to define which browser configurations you want to test, and where Riot and your homeserver is hosted (e.g. localhost).

- Make sure to set `ANNOTATE_STRINGS = true` in `riot-web/src/languageHandler.js` if you want to take screenshots of individual strings.

- Install and configure docker (left as an excercise for the reader).

- Run tests:
`./run.sh`

The `screenshots/` directory will contain all output screenshots.

By default the tests run Synapse using https://gitlab.com/saparvia/synapse-for-tests. If you make additional tests that require interaction, you can add the necessary functionality to the simple bot included in that repository.
