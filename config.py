import selenium

import browser

# Drivers to test
# Firefox might not work due to https://github.com/mozilla/geckodriver/issues/1074
# This should be fixed in Firefox 58
DRIVERS = {
    'chrome-de-1280x1024': (lambda: browser.chrome('de', '1280',1024)),
    #'FF-en-1024x768': (lambda: browser.firefox('en', 1024, 768)),
}

# Wait at most this long for looked-after elements to show up
IMPLICITLY_WAIT = 10

# URL to Riot Web
APP = 'http://localhost:8080/#'

# Homeserver to use
HOMESERVER = 'https://localhost:8448'

# Username and password of test user. It will be created, so probably no need to change this
USERNAME = 'theodortestington'
PASSWORD = 'testtest'
