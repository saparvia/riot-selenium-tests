"""Basic tests for Riot Web"""
import unittest
import time

from browser import browsers

from config import APP

class TestBasic(unittest.TestCase):
    """Unit tests for Riot Web"""
    def test_homepage(self):
        """Navigate to homepage"""
        name = 'homepage'
        for browser in browsers():

            try:
                browser.driver.get(APP)
                browser.driver.find_element_by_class_name('mx_RoleButton') # Implictly sleeps until loaded
                browser.screenshot(name, 'base')
            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()

    def test_login(self):
        """Navigate to login via homepage"""
        name = 'login'
        for browser in browsers():

            try:
                browser.driver.get(APP)
                browser.driver.find_element_by_class_name('mx_LoginBox_loginButton').click()
                browser.driver.find_element_by_class_name('mx_Login_field')
                browser.screenshot(name, 'base')
                browser.click_dropdowns(name, 'base')
                browser.driver.find_element_by_id('advanced').click()
                browser.screenshot(name, 'advanced')
                browser.driver.find_element_by_class_name('mx_ServerConfig_help').click()
                browser.driver.find_element_by_class_name('mx_Dialog_title')
                browser.screenshot(name, 'help')
            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()

    def test_register(self):
        """Navigate to registration via homepage"""
        name = 'register'
        for browser in browsers():

            try:
                browser.driver.get(APP)
                browser.driver.find_element_by_class_name('mx_LoginBox_registerButton').click()
                browser.driver.find_element_by_class_name('mx_Login_field')
                browser.screenshot(name, 'base')
                browser.click_dropdowns(name, 'base')

                browser.driver.find_element_by_class_name('mx_Login_submit').click()
                browser.driver.find_element_by_class_name('error')
                browser.screenshot(name, 'missing-info')
            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()

    def test_settings_unregistered(self):
        """See what happens when we try to open settings without logging in"""
        name = 'settings-unregistered'
        for browser in browsers():

            try:
                browser.driver.get(APP)
                browser.driver.find_element_by_class_name('mx_BottomLeftMenu_settings').click()
                browser.driver.find_element_by_class_name('mx_Dialog_title')
                browser.screenshot(name, 'base')
            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()

    def test_directory(self):
        """Take a look at the directory"""
        name = 'directory'
        for browser in browsers():

            try:
                browser.driver.get(APP)
                # The buttons have no unique class or id so go by index instead. May break
                browser.driver.find_element_by_class_name('mx_BottomLeftMenu_options').find_elements_by_xpath('./div')[2].click()
                browser.driver.find_element_by_class_name('mx_RoomDirectory_topic')
                browser.screenshot(name, 'base')
                browser.click_dropdowns(name, 'base')
            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()

    def test_peek(self):
        """Peek into a room"""
        name = 'peek'
        for browser in browsers():

            try:
                browser.driver.get(APP + "/room/#testroom1:localhost")
                browser.driver.find_element_by_class_name('mx_RoomPreviewBar')
                browser.screenshot(name, 'base')
                buttons = browser.driver.find_element_by_class_name('mx_RightPanel_headerButtonGroup').find_elements_by_xpath('.//div')
                buttons[1].click()
                browser.screenshot(name, 'files')
                buttons[2].click()
                browser.screenshot(name, 'alerts')
            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()
