#!/bin/sh
# Start docker container with Synapse, run some tests, and stop container

SLEEP_TIME=30 # Time to sleep so that server has time to start
DRIVER_DIR=. # Directory where webdrivers are

DOCKER_ID=`docker run -d -p 8448:8448 synapse-for-tests start`

echo "Sleeping for ${SLEEP_TIME}s so that server can start..."
sleep ${SLEEP_TIME} # Wait until server is ready. TODO: Better way to figure this out

echo "Starting test"
PATH=$PATH:${DRIVER_DIR} python3 -m unittest -f -v

docker stop ${DOCKER_ID}
