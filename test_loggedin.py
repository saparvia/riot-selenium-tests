"""Test Riot web functionality which requires that you are logged in"""
import time
import unittest
import os.path

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

from browser import browsers

from config import APP, HOMESERVER, USERNAME, PASSWORD

def setUpModule():
    """Register a new user so that subsequent tests work"""

    # browsers is an iterator, so we need to loop even though we only care about the first one
    for browser in browsers():
        browser.driver.get(APP + '/register')

        # Set custom server
        browser.driver.find_element_by_id('advanced').click()
        hsurl = browser.driver.find_element_by_id('hsurl')
        hsurl.send_keys([Keys.BACKSPACE]*50)
        time.sleep(1) # Wait for typing to be done
        hsurl.send_keys(HOMESERVER)
        time.sleep(1)

        # Type username and password
        fields = browser.driver.find_elements_by_class_name('mx_Login_field')
        fields[2].send_keys(USERNAME)
        fields[3].send_keys(PASSWORD)
        fields[4].send_keys(PASSWORD)

        browser.driver.find_element_by_class_name('mx_Login_submit').click()
        browser.driver.find_element_by_class_name('mx_Dialog_primary').click() # Accept no email
        time.sleep(3)
        browser.driver.quit()
        return

def login(browser):
    """Login to Riot web"""
    browser.driver.get(APP + '/login')

    # Need to fill in server URL first due to various problems
    browser.driver.find_element_by_id('advanced').click()
    time.sleep(1)
    hsurl = browser.driver.find_element_by_id('hsurl')
    hsurl.send_keys([Keys.BACKSPACE]*50)
    time.sleep(2) # Wait for typing to be done
    hsurl.send_keys(HOMESERVER)
    time.sleep(2)

    browser.driver.find_element_by_xpath('//input[@name="username"]').send_keys(USERNAME)
    time.sleep(2)
    browser.driver.find_element_by_xpath('//input[@name="password"]').send_keys(PASSWORD)
    time.sleep(2)

    browser.driver.find_element_by_class_name('mx_Login_submit').click()

def set_room_avatar(browser, file_path):
    browser.driver.find_element_by_class_name('mx_RoomHeader_rightRow').find_elements_by_xpath('.//div')[0].click()
    time.sleep(3)
    browser.driver.find_element_by_id('avatarInput').send_keys(os.path.abspath(file_path))
    browser.driver.find_element_by_class_name('mx_RoomHeader_cancelButton').click()

class TestLoggedin(unittest.TestCase):
    """Unit tests for Riot web logged in functionality"""
    def test_loggedin(self):
        """Basic login test"""
        name = 'loggedin'
        for browser in browsers():

            try:
                login(browser)
                browser.driver.find_element_by_class_name('mx_HomePage_header')
                browser.screenshot(name, 'base')
            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()

    def test_settings(self):
        """Test settings"""
        name = 'settings'
        for browser in browsers():

            try:
                login(browser)
                browser.driver.find_element_by_class_name('mx_BottomLeftMenu_settings').click()
                browser.driver.find_element_by_class_name('mx_UserSettings_body')
                browser.screenshot(name, 'base')
                dark_theme = browser.driver.find_element_by_id('theme_theme_dark_account')
                browser.driver.execute_script("arguments[0].scrollIntoView({});", dark_theme) # Scroll a bit past radio to avoid https://github.com/vector-im/riot-web/issues/5686
                dark_theme.click()
                browser.screenshot(name, 'dark')
                light_theme = browser.driver.find_element_by_id('theme_theme_light_account')
                browser.driver.execute_script("arguments[0].scrollIntoView({});", light_theme)
                light_theme.click()
                time.sleep(2) # Takes a while to actually switch
                browser.screenshot(name, 'light')
            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()

    def test_joinroom(self):
        """Test joining a room"""
        name = 'join-room'
        for browser in browsers():

            try:
                login(browser)

                browser.driver.get(APP)

                # Join a room
                browser.driver.find_element_by_class_name('mx_BottomLeftMenu_options').find_elements_by_xpath('./div')[2].click()
                browser.screenshot(name, 'directory')
                browser.driver.find_element_by_class_name('mx_RoomDirectory_name').click()
                browser.driver.find_element_by_class_name('mx_RoomPreviewBar_join_text')
                time.sleep(30)
                browser.screenshot(name, 'preview')
                browser.driver.find_element_by_xpath('//div[@class="mx_RoomPreviewBar_join_text"]//a').click()
                time.sleep(50)
                browser.screenshot(name, 'joined')

                # Check settings
                browser.driver.find_element_by_class_name('mx_RoomHeader_rightRow').find_elements_by_xpath('.//div')[0].click()
                time.sleep(3)
                browser.driver.find_element_by_class_name('mx_RoomSettings_leaveButton')
                browser.screenshot(name, 'settings')
                browser.driver.find_element_by_class_name('mx_RoomSettings_leaveButton').click()
                browser.driver.find_element_by_class_name('mx_QuestionDialog')

                # Leave room
                browser.screenshot(name, 'confirm-leave')
                browser.driver.find_element_by_class_name('mx_Dialog_primary').click()
            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()

    def test_createroom(self):
        """Test creating a room"""
        name = 'create-room'
        for browser in browsers():

            try:
                login(browser)

                # Create a new room
                browser.driver.find_element_by_class_name('mx_BottomLeftMenu_options').find_elements_by_xpath('./div')[3].click()
                browser.driver.find_element_by_class_name('mx_CreateRoomDialog_input').send_keys('Test room')
                browser.screenshot(name, 'create')
                browser.driver.find_element_by_tag_name('details').click()
                browser.screenshot(name, 'advanced')
                browser.driver.find_element_by_class_name('mx_Dialog_primary').click()
                browser.driver.find_element_by_class_name('mx_DateSeparator')
                set_room_avatar(browser, 'room_avatar.png')
                browser.screenshot(name, 'created')

                # Invite Testbot 
                browser.driver.find_element_by_class_name('mx_RightPanel_invite').click()
                browser.driver.find_element_by_class_name('mx_ChatInviteDialog_input').send_keys('@testbot:localhost')
                browser.screenshot(name, 'invite')
                browser.driver.find_element_by_class_name('mx_Dialog_primary').click()
                browser.screenshot(name, 'invited')
                browser.driver.find_element_by_xpath('//span[@class="mx_EventTile_body" and contains(., "Hi there!")]')
                browser.screenshot(name, 'invited-joined')

                # Kick Testbot
                browser.driver.find_element_by_xpath('//div[@class="mx_EntityTile_name" and contains(., "Testbot!")]').click()
                browser.screenshot(name, 'sidebar-userinfo')
                browser.driver.find_elements_by_class_name('mx_MemberInfo_buttons')[1].find_elements_by_class_name('mx_MemberInfo_field')[1].click()
                browser.driver.find_element_by_class_name('mx_ConfirmUserActionDialog_reasonField').send_keys('This is a test kick!')
                browser.screenshot(name, 'kick')
                browser.driver.find_element_by_class_name('mx_Dialog_primary').click()
                time.sleep(2)
                browser.screenshot(name, 'kicked')

                browser.driver.find_element_by_class_name('public-DraftEditor-content').send_keys('/part')
                browser.driver.find_element_by_class_name('public-DraftEditor-content').send_keys(Keys.RETURN)
            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()

    def test_writing(self):
        """Test creating a room"""
        name = 'writing'
        for browser in browsers():

            try:
                login(browser)

                browser.driver.get(APP)

                # Create a new room
                browser.driver.find_element_by_class_name('mx_BottomLeftMenu_options').find_elements_by_xpath('./div')[3].click()
                browser.driver.find_element_by_class_name('mx_Dialog_primary').click()
                set_room_avatar(browser, 'room_avatar.png')

                # Redact text
                browser.driver.find_element_by_class_name('public-DraftEditor-content').send_keys('Redact this')
                browser.driver.find_element_by_class_name('public-DraftEditor-content').send_keys(Keys.RETURN)
                time.sleep(2)
                last_msg = browser.driver.find_elements_by_class_name('mx_EventTile')[-1]
                hover = ActionChains(browser.driver).move_to_element(last_msg)
                hover.perform()
                time.sleep(1)
                last_msg.find_element_by_class_name('mx_EventTile_editButton').click()
                time.sleep(1)
                menu = browser.driver.find_element_by_id('mx_ContextualMenu_Container').find_elements_by_class_name('mx_MessageContextMenu_field')
                browser.screenshot(name, 'message-menu')
                menu[0].click()
                time.sleep(1)
                browser.driver.find_element_by_class_name('mx_QuestionDialog')
                browser.screenshot(name, 'redact')
                browser.driver.find_element_by_class_name('mx_Dialog_primary').click()
                browser.driver.find_element_by_class_name('mx_UnknownBody')
                browser.screenshot(name, 'redacted')

                # Write some complicated text using the RTE
                editor = browser.driver.find_element_by_class_name('public-DraftEditor-content')
                browser.driver.find_element_by_class_name('mx_MessageComposer_formatting').click()
                format_buttons = browser.driver.find_elements_by_class_name('mx_MessageComposer_format_button')

                format_buttons[0].click()
                editor.send_keys('bold')
                editor.send_keys([Keys.RIGHT]*2)

                format_buttons[1].click()
                editor.send_keys('italic')
                browser.driver.find_element_by_class_name('public-DraftEditor-content').send_keys([Keys.RIGHT]*1)

                format_buttons[2].click()
                editor.send_keys('strike')
                editor.send_keys([Keys.RIGHT]*6)

                format_buttons[3].click()
                editor.send_keys('underline')
                editor.send_keys([Keys.RIGHT]*4)

                editor.send_keys([Keys.SHIFT, Keys.ENTER]) # Need to make new line ourselves

                format_buttons[4].click()
                editor.send_keys('code')
                editor.send_keys([Keys.DOWN]*2)

                format_buttons[5].click()
                editor.send_keys('quote')
                editor.send_keys([Keys.DOWN]*2)

                format_buttons[6].click()
                editor.send_keys('bullet')

                format_buttons[7].click()
                editor.send_keys('numbullet')

                browser.screenshot(name, 'long-compose')
                editor.send_keys([Keys.ENTER])

                browser.screenshot(name, 'markdown')

            except:
                browser.screenshot(name, 'error')
                raise
            finally:
                browser.driver.quit()
